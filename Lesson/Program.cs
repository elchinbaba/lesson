﻿using System;

namespace Lesson
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Student student = new(fullName: "Hafiz Haciyev", faculty: "ITIF", absentHours: 5);

                int[] midtermResults = { 11, 14, 17 };
                int[] laboratoryResults = { 1, 0, 1, 1, 0, 0, 1, 1, 1, 0 };
                int presentationResult = 5;
                int quizResult = 7;

                Subject subject = new(
                    name: "Calculus",
                    classHours: 22,
                    student: student,
                    midtermResults: midtermResults,
                    laboratoryResults: laboratoryResults,
                    presentationResult: presentationResult,
                    quizResult: quizResult
                );
                Console.WriteLine($"Student {student.FullName} from {student.Faculty} to {subject.Name} exam:");
                Console.WriteLine(subject.FindEntranceScoreToExam());

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
