﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson
{
    class Student
    {
        private int _absentHours;
        public string FullName { get; set; }
        public string Faculty { get; set; }

        public Student() { }

        public Student(
            string fullName,
            string faculty,
            int absentHours
            )
        {
            FullName = fullName;
            Faculty = faculty;
            AbsentHours = absentHours;
        }
        public int AbsentHours
        {
            get
            {
                return _absentHours;
            }

            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Absent hours can not be under 0");
                }
                else
                {
                    _absentHours = value;
                }
            }
        }

    }
}
