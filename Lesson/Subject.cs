﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson
{
    class Subject
    {
        private Student _student;
        private int _classHours;
        private int[] _midtermResults;
        private int[] _laboratoryResults;
        private int _presentationResult;
        private int _quizResult;

        public Subject(
            string name,
            int classHours,
            Student student,
            int[] midtermResults,
            int[] laboratoryResults,
            int presentationResult,
            int quizResult
            )
        {
            Name = name;
            ClassHours = classHours;
            Student = student;
            MidtermResults = midtermResults;
            LaboratoryResults = laboratoryResults;
            PresentationResult = presentationResult;
            QuizResult = quizResult;
        }

        public string Name { get; set; }
        public Student Student
        {
            get
            {
                return _student;
            }
            set
            {
                _student = value ?? throw new Exception("Student can not be null");
            }
        }
        public int ClassHours
        {
            get
            {
                return _classHours;
            }

            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Class hours can not be under 0");
                }
                _classHours = value;
            }
        }

        public int[] MidtermResults
        {
            get
            {
                return _midtermResults;
            }

            set
            {
                if (!ValidateMidtermResults(value))
                {
                    throw new Exception("Each midterm result might either be 0 and 1. 3 midterm exams exist.");
                }
                _midtermResults = value;
            }
        }

        public int[] LaboratoryResults
        {
            get
            {
                return _laboratoryResults;
            }

            set
            {
                if (!ValidateLaboratoryResults(value))
                {
                    throw new Exception("Each laboratory result might either be 0 and 1. 10 laboratory lessons exist.");
                }
                _laboratoryResults = value;
            }
        }

        public int PresentationResult
        {
            get
            {
                return _presentationResult;
            }

            set
            {
                if (!ValidatePresentationOrQuizResult(value))
                {
                    throw new ArgumentException("Presentation result must be between 0 and 10!");
                }
                _presentationResult = value;
            }
        }

        public int QuizResult
        {
            get
            {
                return _quizResult;
            }

            set
            {
                if (!ValidatePresentationOrQuizResult(value))
                {
                    throw new ArgumentException("Quiz result must be between 0 and 10!");
                }
                _quizResult = value;
            }
        }

        public int FindEntranceScoreToExam()
        {
            static int getMidtermMark(int[] midtermResults)
            {
                double sum = 0;
                foreach (int midtermResult in midtermResults)
                {
                    sum += midtermResult;
                }

                return (int)Math.Ceiling(sum / 3);
            }
            
            static int getLaboratoryMark(int[] laboratoryResults)
            {
                int sum = 0;
                foreach (int laboratoryResult in laboratoryResults)
                {
                    sum += laboratoryResult;
                }

                return sum;
            }
            if (Student.AbsentHours >= ClassHours / 4.0)
            {
                throw new Exception("Student can not enter to the exam due to their absent lessons");
            }

            int midtermMark = getMidtermMark(MidtermResults);
            int laboratoryMark = getLaboratoryMark(LaboratoryResults);

            return midtermMark + laboratoryMark + PresentationResult + QuizResult;
        }

        static bool ValidateMidtermResults(int[] midtermResults)
        {
            if (midtermResults.Length != 3)
            {
                return false;
            }

            foreach (int midtermResult in midtermResults)
            {
                if (midtermResult > 20 || midtermResult < 0)
                {
                    return false;
                }
            }

            return true;
        }

        static bool ValidateLaboratoryResults(int[] laboratoryResults)
        {
            if (laboratoryResults.Length != 10)
            {
                return false;
            }

            foreach (int laboratoryResult in laboratoryResults)
            {
                if (laboratoryResult != 1 && laboratoryResult != 0)
                {
                    return false;
                }
            }

            return true;
        }

        static bool ValidatePresentationOrQuizResult(int result)
        {
            static bool isResultIn0and10(int result)
            {
                return result <= 10 && result >= 0;
            }

            return isResultIn0and10(result);
        }
    }
}
